/*********************************************************************
* Filename:   dh.c
* Author:     Zachary Ho (adapted from Lab 5 client.c)
*********************************************************************/

#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include "sha256.h"

int mod_func(int base, int exponent, int modulus);

int main(int argc, char **argv)
{
  int sockfd, portno, n;
  struct sockaddr_in serv_addr;
  struct hostent *server;

  char buffer[10000];

  portno = 7800;

  /* Translate host name into peer's IP address ;
     * This is name translation service by the operating system */
  server = gethostbyname("172.26.37.44");

  if (server == NULL)
  {
    fprintf(stderr, "ERROR, no such host\n");
    exit(EXIT_FAILURE);
  }

  /* Building data structures for socket */
  bzero((char *)&serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy(server->h_addr_list[0], (char *)&serv_addr.sin_addr.s_addr, server->h_length);
  serv_addr.sin_port = htons(portno);

  /* Create TCP socket -- active open
     * Preliminary steps: Setup: creation of active open socket */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
  {
    perror("ERROR opening socket");
    exit(EXIT_FAILURE);
  }

  if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    perror("ERROR connecting");
    exit(EXIT_FAILURE);
  }

  /* Do processing */

  // Send username to server
  strcpy(buffer, "yho4\n");
  printf("buffer is %s\n", buffer);

  n = write(sockfd, buffer, strlen(buffer));
  if (n < 0)
  {
    perror("ERROR writing to socket");
    exit(EXIT_FAILURE);
  }

  FILE *dh_file_ptr_2 = popen("openssl sha256 dh.c", "r");
  char new_buffer[256];
  fgets(new_buffer, 100, dh_file_ptr_2);

  char new_hexa[256];
  char digit_2[3];
  digit_2[2] = '\0';
  strcpy(new_hexa, strstr(new_buffer, "= ") + 2);
  strncpy(digit_2, new_hexa, 2);

  int b = strtol(digit_2, NULL, 16);
  int g = 15;
  int p = 97;
  int gbmodp = mod_func(g, b, p);
  char gbmodp_text[10];
  sprintf(gbmodp_text, "%d\n", gbmodp);

  n = write(sockfd, gbmodp_text, strlen(gbmodp_text));
  if (n < 0)
  {
    perror("ERROR writing to socket");
    exit(EXIT_FAILURE);
  }

  n = read(sockfd, buffer, 255);
  if (n < 0)
  {
    perror("ERROR reading from socket");
    exit(EXIT_FAILURE);
  }

  int buffer_int = atoi(buffer);

  int gbamodp = mod_func(buffer_int, b, p);
  char gbamodp_text[10];
  sprintf(gbamodp_text, "%d\n", gbamodp);
  buffer[n] = 0;

  n = write(sockfd, gbamodp_text, strlen(gbamodp_text));
  if (n < 0)
  {
    perror("ERROR writing to socket");
    exit(EXIT_FAILURE);
  }

  n = read(sockfd, buffer, 255);
  if (n < 0)
  {
    perror("ERROR reading from socket");
    exit(EXIT_FAILURE);
  }

  printf("buffer is finally %s\n", buffer);

  close(sockfd);

  return 0;
}

// Taken and adapted from https://www.mtholyoke.edu/courses/quenell/s2003/ma139/js/powermod.html's javascript
int mod_func(int base, int exponent, int modulus)
{
  if ((base < 1) || (exponent < 0) || (modulus < 1))
  {
    return 0;
  }
  int result = 1;
  while (exponent > 0)
  {
    if ((exponent % 2) == 1)
    {
      result = (result * base) % modulus;
    }
    base = (base * base) % modulus;
    exponent = floor(exponent / 2);
  }
  return result;
}
