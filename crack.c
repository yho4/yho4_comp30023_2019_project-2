/*********************************************************************
* Filename:   crack.c
* Author:     Zachary Ho (adapted from Brad Conte (brad AT bradconte.com))
*********************************************************************/

/*************************** HEADER FILES ***************************/
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <stdlib.h>
#include "sha256.h"
#include <limits.h>

/*********************** FUNCTION DEFINITIONS ***********************/
int sha256_test(int argc, char *argv[])
{
  // 0-argument version
  if (argc == 1)
  {
    FILE *sha_file_4_ptr = fopen("pwd4sha256", "r");
    BYTE hash4[SHA256_BLOCK_SIZE];
    FILE *sha_file_6_ptr = fopen("pwd6sha256", "r");
    BYTE hash6[SHA256_BLOCK_SIZE];

    int all_ascii[95];
    int current_ascii = 32;
    for (int i = 0; i < 95; i++)
    {
      all_ascii[i] = current_ascii;
      current_ascii++;
    }

    // For 4-digit passwords
    int hash_number = 1;
    // Keep reading the pwd4sha256 file until we get all 10 hashes
    while (fread(hash4, sizeof(BYTE), SHA256_BLOCK_SIZE, sha_file_4_ptr))
    {
      BYTE buf4[SHA256_BLOCK_SIZE];
      SHA256_CTX ctx4;

      // Variables to keep track of looping int values
      int digit_1;
      int digit_2;
      int digit_3;
      int digit_4;

      char *guess = malloc(4 * sizeof(char));
      // FOR loops to generate all possible 4-digit passwords
      for (digit_1 = 0; digit_1 < 95; digit_1++)
      {
        for (digit_2 = 0; digit_2 < 95; digit_2++)
        {
          for (digit_3 = 0; digit_3 < 95; digit_3++)
          {
            for (digit_4 = 0; digit_4 < 95; digit_4++)
            {
              guess[0] = all_ascii[digit_1];
              guess[1] = all_ascii[digit_2];
              guess[2] = all_ascii[digit_3];
              guess[3] = all_ascii[digit_4];

              BYTE *byte_guess = (BYTE *)guess;

              sha256_init(&ctx4);
              sha256_update(&ctx4, byte_guess, 4);
              sha256_final(&ctx4, buf4);

              // Print when we find passwords matching hashes
              if (!memcmp(hash4, buf4, SHA256_BLOCK_SIZE))
              {
                printf("%s %d\n", guess, hash_number);
              }
            }
          }
        }
      }
      hash_number++;
    }

    // For 6-digit passwords
    hash_number = 11;
    // Keep reading the pwd6sha256 file until we get all hashes
    while (fread(hash6, sizeof(BYTE), SHA256_BLOCK_SIZE, sha_file_6_ptr))
    {
      BYTE buf6[SHA256_BLOCK_SIZE];
      SHA256_CTX ctx6;

      // Variables to keep track of looping int values
      int digit_1;
      int digit_2;
      int digit_3;
      int digit_4;
      int digit_5;
      int digit_6;

      char *guess = malloc(6 * sizeof(char));
      // FOR loops to generate all possible 6-digit passwords
      for (digit_1 = 0; digit_1 < 95; digit_1++)
      {
        for (digit_2 = 0; digit_2 < 95; digit_2++)
        {
          for (digit_3 = 0; digit_3 < 95; digit_3++)
          {
            for (digit_4 = 0; digit_4 < 95; digit_4++)
            {
              for (digit_5 = 0; digit_5 < 95; digit_5++)
              {
                for (digit_6 = 0; digit_6 < 95; digit_6++)
                {
                  guess[0] = all_ascii[digit_1];
                  guess[1] = all_ascii[digit_2];
                  guess[2] = all_ascii[digit_3];
                  guess[3] = all_ascii[digit_4];
                  guess[4] = all_ascii[digit_5];
                  guess[5] = all_ascii[digit_6];

                  BYTE *byte_guess = (BYTE *)guess;

                  sha256_init(&ctx6);
                  sha256_update(&ctx6, byte_guess, 6);
                  sha256_final(&ctx6, buf6);

                  // Print when we find passwords matching hashes
                  if (!memcmp(hash6, buf6, SHA256_BLOCK_SIZE))
                  {
                    printf("%s %d\n", guess, hash_number);
                  }
                }
              }
            }
          }
        }
      }
      hash_number++;
    }
    fclose(sha_file_4_ptr);
    fclose(sha_file_6_ptr);
  }

  // 1-argument version
  else if (argc == 2)
  {
    int number_guesses = atoi(argv[1]);
    int guessed = 0;

    int all_ascii[95];
    int current_ascii = 32;
    for (int i = 0; i < 95; i++)
    {
      all_ascii[i] = current_ascii;
      current_ascii++;
    }

    // Brute force way to generate argv[1] (up to 6-digit) passwords
    int digit_1;
    int digit_2;
    int digit_3;
    int digit_4;
    int digit_5;
    int digit_6;

    char *guess = malloc(6 * sizeof(char));
    for (digit_1 = 0; digit_1 < 95; digit_1++)
    {
      for (digit_2 = 0; digit_2 < 95; digit_2++)
      {
        for (digit_3 = 0; digit_3 < 95; digit_3++)
        {
          for (digit_4 = 0; digit_4 < 95; digit_4++)
          {
            for (digit_5 = 0; digit_5 < 95; digit_5++)
            {
              for (digit_6 = 0; digit_6 < 95; digit_6++)
              {
                guess[0] = all_ascii[digit_1];
                guess[1] = all_ascii[digit_2];
                guess[2] = all_ascii[digit_3];
                guess[3] = all_ascii[digit_4];
                guess[4] = all_ascii[digit_5];
                guess[5] = all_ascii[digit_6];

                printf("%s\n", guess);
                guessed++;
                if (guessed == number_guesses)
                {
                  return 0;
                }
              }
            }
          }
        }
      }
    }
  }

  // 2-argument version
  else if (argc == 3)
  {
    FILE *sha_file_ptr = fopen(argv[2], "r");
    BYTE hash[SHA256_BLOCK_SIZE];
    SHA256_CTX ctx;

    // Finding size of hash file
    fseek(sha_file_ptr, 0, SEEK_END);
    int size = ftell(sha_file_ptr);
    fseek(sha_file_ptr, 0, SEEK_SET);

    // Allocating memory to store hash file
    BYTE *hashes = malloc(size * sizeof(BYTE));
    fread(hashes, sizeof(BYTE), size, sha_file_ptr);
    fclose(sha_file_ptr);

    FILE *pwd_list_ptr = fopen(argv[1], "r");
    char buf[PASSWORD_MAX_LENGTH];
    while (fgets(buf, PASSWORD_MAX_LENGTH, pwd_list_ptr))
    {
      char *pos;
      if ((pos = strchr(buf, '\n')) != NULL)
      {
        *pos = '\0';
      }

      BYTE *byte_pwd = (BYTE *)buf;

      sha256_init(&ctx);
      sha256_update(&ctx, byte_pwd, strlen(buf));
      sha256_final(&ctx, hash);

      int hash_number = 1;
      for (int i = 0; i < size; i = i + SHA256_BLOCK_SIZE)
      {

        // Print when we find passwords matching hashes
        if (!memcmp(hashes + i, hash, SHA256_BLOCK_SIZE))
        {
          printf("%s %d\n", buf, hash_number);
        }
        hash_number++;
      }
    }
  }
  return 0;
}

int main(int argc, char *argv[])
{
  sha256_test(argc, argv);
  return (0);
}
