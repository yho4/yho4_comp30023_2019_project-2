/*********************************************************************
* Filename:   crack.c
* Author:     Brad Conte (brad AT bradconte.com)
* Copyright:
* Disclaimer: This code is presented "as is" without any guarantees.
* Details:    Performs known-answer tests on the corresponding SHA1
	          implementation. These tests do not encompass the full
	          range of available test vectors, however, if the tests
	          pass it is very, very likely that the code is correct
	          and was compiled properly. This code also serves as
	          example usage of the functions.
*********************************************************************/

/*************************** HEADER FILES ***************************/
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <stdlib.h>
#include "sha256.h"

/*********************** FUNCTION DEFINITIONS ***********************/
int sha256_test()
{
    FILE *sha_file_ptr = fopen("pwd4sha256", "r");
    BYTE hash[SHA256_BLOCK_SIZE];

    /********** Below is code for getting ASCII values for numbers, lowercase, and uppercase **********/
    // ASCII values for numbers, lowercase letters (97 - 122), and uppercase letters (65-90)
    // int num_ascii[10] = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57};
    // int lowercase_ascii[26];
    // int uppercase_ascii[26];
    // int current_lower_ascii = 97;
    // int current_upper_ascii = 65;
    // for (int i = 0; i < 26; i++)
    // {
    //     lowercase_ascii[i] = current_lower_ascii;
    //     uppercase_ascii[i] = current_upper_ascii;
    //     current_lower_ascii++;
    //     current_upper_ascii++;
    // }

    // // Put all wanted ASCII values together
    // int* all_ascii = malloc(62 * sizeof(int));
    // memcpy(all_ascii,  num_ascii, 10 * sizeof(int));
    // memcpy(all_ascii + 10, lowercase_ascii, 26 * sizeof(int));
    // memcpy(all_ascii + 36, uppercase_ascii, 26 * sizeof(int));

    /********** Below is code for all relevant ASCII characters **********/
    int all_ascii[95];
    int current_ascii = 32;
    for (int i = 0; i < 95; i++)
    {
        all_ascii[i] = current_ascii;
        current_ascii++;
    }

    int hash_number = 0;
    // Keep reading the pwd4sha256 file until we get all 10 hashes
    while (fread(hash, sizeof(BYTE), SHA256_BLOCK_SIZE, sha_file_ptr))
    {
        BYTE buf[SHA256_BLOCK_SIZE];
        SHA256_CTX ctx;
        
        // Variables to keep track of looping int values
        int digit_1;
        int digit_2;
        int digit_3;
        int digit_4;

        char* guess = malloc(4 * sizeof(char));
        // FOR loops to generate all possible 4-digit passwords (a-z)
        // First digit
        for (digit_1 = 0; digit_1 < 95; digit_1++)
        {
            for (digit_2 = 0; digit_2 < 95; digit_2++)
            {
                for (digit_3 = 0; digit_3 < 95; digit_3++)
                {
                    for (digit_4 = 0; digit_4 < 95; digit_4++)
                    {
                        guess[0] = all_ascii[digit_1];
                        guess[1] = all_ascii[digit_2];
                        guess[2] = all_ascii[digit_3];
                        guess[3] = all_ascii[digit_4];
                        // guess[4] = '\0';

                        BYTE* byte_guess = (BYTE *)guess;
                        // printf("guess[4] is %s\n", guess);
                        // printf("digit_4 is %d\n", digit_4);
                        
                        sha256_init(&ctx);
                        sha256_update(&ctx, byte_guess, 4);
                        sha256_final(&ctx, buf);

                        if (!memcmp(hash, buf, SHA256_BLOCK_SIZE))
                        {
                            printf("%s %d\n", guess, hash_number);
                        }
                        // pass = pass && !memcmp(hash, buf, SHA256_BLOCK_SIZE);
                    }
                }
            }
        }
    hash_number++;
    // }

    // // char username[] = "yho4";
    // // BYTE *byte_username = (BYTE *)username;

    

    // // sha256_init(&ctx);
    // // sha256_update(&ctx, text2, strlen(text2));
    // // sha256_final(&ctx, buf);
    // // pass = pass && !memcmp(hash2, buf, SHA256_BLOCK_SIZE);

    // // sha256_init(&ctx);
    // // for (idx = 0; idx < 100000; ++idx)
    // //     sha256_update(&ctx, text3, strlen(text3));
    // // sha256_final(&ctx, buf);
    // // pass = pass && !memcmp(hash3, buf, SHA256_BLOCK_SIZE);

    // return (pass);
    }
    return (0);

}

int main()
{
    printf("SHA-256 tests: %s\n", sha256_test() ? "SUCCEEDED" : "FAILED");

    return (0);
}
