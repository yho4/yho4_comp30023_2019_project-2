# CC=gcc
# CFLAGS=-Wall
# EXE=crack

# $(EXE): sha256.c crack.c
#     $(CC) $(CFLAGS) $^ -o $(EXE)

# clean:
# 	rm $(EXE)

crack: sha256.c crack.c
	gcc -Wall crack.c sha256.c -o crack