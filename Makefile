all: crack dh

crack: sha256.c crack.c
	gcc -Wall -g crack.c sha256.c -o crack

dh: dh.c sha256.c
	gcc -Wall -g dh.c sha256.c -o dh